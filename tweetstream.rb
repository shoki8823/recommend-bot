require 'tweetstream'
require 'twitter'
require 'amazon/ecs'


# twitter config
# Consumer key, Secret config
CONSUMER_KEY     = "YOUR CONSUMER KEY"
CONSUMER_SECRET  = "YOUR CONSUMER SECRET KEY"
# Access Token Key, Secret config
ACCESS_TOKEN_KEY = "YOUR ACCESS TOKEN KEY"
ACCESS_SECRET    = "YOUR ACCESS SECRET KEY"


#amazon api config
ASSOCIATE_TAG = 'YOUR ASSOCIATE TAG'
AWS_ACCESS_KEY_ID = 'YOUR ACCESS KEY ID'
AWS_SECRET_KEY = 'AWS_SECRET_KEY'


#twitter client config set
client = Twitter::REST::Client.new do |config|
  config.consumer_key        = CONSUMER_KEY
  config.consumer_secret     = CONSUMER_SECRET
  config.access_token        = ACCESS_TOKEN_KEY
  config.access_token_secret = ACCESS_SECRET
end

#twitter streamingApi config set
TweetStream.configure do |config|
  config.consumer_key       = CONSUMER_KEY
  config.consumer_secret    = CONSUMER_SECRET
  config.oauth_token        = ACCESS_TOKEN_KEY
  config.oauth_token_secret = ACCESS_SECRET
  config.auth_method        = :oauth
end

#amazon api config set
Amazon::Ecs.options = {
	:associate_tag => ASSOCIATE_TAG,
	:AWS_access_key_id => AWS_ACCESS_KEY_ID,
	:AWS_secret_key => AWS_SECRET_KEY
}

#streaming api listen
TweetStream::Client.new.userstream do |status|
  targetId = status.user.screen_name
  puts targetId
  begin
  	# request to amazon api
	res = Amazon::Ecs.item_search(status.text[15,20], {:search_index => 'Books', :response_group => 'Medium', :country => 'jp',:sort => 'salesrank'})

	if (res.items.first.nil? === false) 
	  	h = res.items.first.get_hash
	  	client.update("@" + targetId + " " + "http://amazon.jp/dp/" + h["ASIN"], in_reply_to_status_id: status.id )
	else 
		client.update("@" + targetId + " " + " sorry I could not find any books! ")
	end
  rescue Exception => e
  	puts (e)
  end


end


